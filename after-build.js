const zipFolder = require('zip-folder');

const runScript = () => {
    console.log('Zipping Folder into root');
    zipFolder('./dist/', './dist.zip', (err) => {
        if (err){
            process.exit(2);
            return console.error(err);
        }
        console.log('Succesfully created dist.zip in the root');
        process.exit(0);
    });
};

runScript();