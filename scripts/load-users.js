const User = require('../app/models/user');
const mongoose = require('mongoose');
const config = require('../config');
/**
 * Creates a default User in the Database
 * 
 */
const createUser = () => {
    console.log('Creating Default User admin on ' + config.mongoHost + ':' + config.mongoPort + '/' + config.mongoDb);
    const user = {
        name: 'admin',
        email: 'djdiox@gmail.com',
        hashed_password: User.generatePassword('admin'),
        roles: ['admin']
    };
    const dbUser = new User(user);
    User.findOne({ email: user.email }, (err, res) => {
        if (err) throw err;
        if (res) {
            console.log('User is already created');
            return process.exit(0);
        }
        dbUser.save()
            .then((res) => {
                console.log('Created User in Database', res);
                process.exit(0);
            }, (err) => {
                console.error(err);
                process.exit(0);
            });
    });

};

mongoose.connect(config.mongoUrl,
    null, createUser);