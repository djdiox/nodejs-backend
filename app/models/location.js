'use strict';
/* 
 * Module dependencies
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
* LocationModel schema
*/

const LocationSchema = new Schema({
    name: { type: String, default: '' },
    description: { type: String },
    latitude: { type: Number },
    longitude: { type: Number },
    user_id: { type: String },
    created_at: { type: Date },
    edited_at: { type: Date }
});

/**
 * Register
 */
let LocationModel;
try {
    LocationModel = mongoose.model('Location'); // model is already initialized
} catch (err) {
    LocationModel = mongoose.model('Location', LocationSchema); // model needs to be initialized
}

module.exports = LocationModel;
