'use strict';
/* 
 * Module dependencies
 */
const mongoose = require('mongoose'),
  userPlugin = require('mongoose-user'),
  bcrypt = require('bcrypt-nodejs'),
  Schema = mongoose.Schema;

/**
 * User schema
 */

const UserSchema = new Schema({
  name: { type: String, default: '' },
  email: { type: String, default: '' },
  hashed_password: { type: String, default: '' },
  roles: { type: Array, default: [] },
  local: { type: Object, default: {} },
  twitter: { type: Object, default: {} },
  facebook: { type: Object, default: {} },
  google: { type: Object, default: {} },
});

/**
 * User plugin
 */

UserSchema.plugin(userPlugin, {});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

UserSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.hashed_password);
};


/**
 * Statics
 */

UserSchema.static({
  generatePassword: (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
  },
});

/**
 * Register
 */
let User;
try {
  User = mongoose.model('User'); // model is already initialized
} catch (err) {
  User = mongoose.model('User', UserSchema); // model needs to be initialized
}

module.exports = User;