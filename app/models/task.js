'use strict';
/* 
 * Module dependencies
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
* Task schema
*/

const TaskSchema = new Schema({
    name: { type: String, default: '' },
    start: { type: Date },
    end: { type: Date },
    user_id: { type: String },
    created_at: { type: Date },
    edited_at: { type: Date }
});

/**
 * Register
 */
let Task;
try {
    Task = mongoose.model('Task'); // model is already initialized
} catch (err) {
    Task = mongoose.model('Task', TaskSchema); // model needs to be initialized
}

module.exports = Task;
