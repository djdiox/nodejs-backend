'use strict';
/* 
 * Module dependencies
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
* Todo schema
*/

const TodoSchema = new Schema({
    name: { type: String, default: '' },
    start: { type: Date },
    end: { type: Date },
    user_id: { type: String },
    created_at: { type: Date },
    edited_at: { type: Date }
});

/**
 * Register
 */
let Todo;
try {
    Todo = mongoose.model('Todo'); // model is already initialized
} catch (err) {
    Todo = mongoose.model('Todo', TodoSchema); // model needs to be initialized
}

module.exports = Todo;