'use strict';
/* 
 * Module dependencies
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
* WorkTime schema
*/

const WorkTimeSchema = new Schema({
    name: { type: String, default: '' },
    start: { type: Date },
    end: { type: Date },
    user_id: { type: String },
    created_at: { type: Date },
    edited_at: { type: Date }
});

/**
 * Register
 */
let WorkTime;
try {
    WorkTime = mongoose.model('WorkTime'); // model is already initialized
} catch (err) {
    WorkTime = mongoose.model('WorkTime', WorkTimeSchema); // model needs to be initialized
}

module.exports = WorkTime;