'use strict';
/* 
 * Module dependencies
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
* Sound schema
*/

const SoundSchema = new Schema({
    name: { type: String, default: '' },
    user_id: { type: Date },
    created_at: { type: Date },
    edited_at: { type: Date }
});

/**
 * Register
 */
let Sound;
try {
    Sound = mongoose.model('Sound'); // model is already initialized
} catch (err) {
    Sound = mongoose.model('Sound', SoundSchema); // model needs to be initialized
}

module.exports = Sound;