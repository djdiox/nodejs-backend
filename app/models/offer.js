'use strict';
/* 
 * Module dependencies
 */
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
* Offer schema
*/

const OfferSchema = new Schema({
    name: { type: String, default: '' },
    date: { type: Date },
    user_id: { type: String },
    created_at: { type: Date },
    edited_at: { type: Date }
});

/**
 * Register
 */
let Offer;
try {
    Offer = mongoose.model('Offer'); // model is already initialized
} catch (err) {
    Offer = mongoose.model('Offer', OfferSchema); // model needs to be initialized
}

module.exports = Offer;
