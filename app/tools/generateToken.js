'use strict';
const jwt = require('jsonwebtoken'),
    environment = require('../../config/index');

/**
 * Utility for generating a JWT token and binding it to a user.
 * @param {User} user
 */
const generateToken = (user) => {
    return jwt.sign(user.toObject(), environment.secretKey, {
        expiresIn: environment.tokenValidityTime
    });
};
module.exports = generateToken;