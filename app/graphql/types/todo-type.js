'use strict';
const {
    // These are the basic GraphQL types need in this tutorial
    GraphQLString,
    GraphQLObjectType,
    // This is used to create required fileds and arguments
    GraphQLNonNull,
    // This is the class we need to create the schema
  } = require('graphql');


module.exports = {
    handlers: () => {
        return new GraphQLObjectType({ // since we declare it here our API will be build with this model
            name: 'Todo',
            description: 'This represents a Todo Item from our list',
            fields: () => ({
                _id: { type: new GraphQLNonNull(GraphQLString) },
                title: { type: new GraphQLNonNull(GraphQLString) },
                body: { type: GraphQLString },
                // author: {
                //     type: UserType,
                //     resolve: function (post) {
                //         return _.find(Authors, a => a.id == post.author_id);
                //     }
                // }
            })
        });
    }
};