'use strict';
const {
    // These are the basic GraphQL types need in this tutorial
    GraphQLString,
    GraphQLFloat,
    GraphQLObjectType,
    // This is used to create required fileds and arguments
    GraphQLNonNull,
    // This is the class we need to create the schema
  } = require('graphql');


module.exports = {
    handlers: () => {
        return new GraphQLObjectType({ // since we declare it here our API will be build with this model
            name: 'Location',
            description: 'This represents a Location',
            fields: () => ({
                _id: { type: new GraphQLNonNull(GraphQLString) },
                name: { type: new GraphQLNonNull(GraphQLString) },
                description: { type: GraphQLString },
                longitude: { type: GraphQLFloat },
                latitude: { type: GraphQLFloat },
                // author: {
                //     type: UserType,
                //     resolve: function (post) {
                //         return _.find(Authors, a => a.id == post.author_id);
                //     }
                // }
            })
        });
    }
};