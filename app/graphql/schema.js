'use strict';
const {
    GraphQLObjectType,
    // This is used to create required fileds and arguments
    GraphQLList,
    // This is the class we need to create the schema
    GraphQLSchema
} = require('graphql');
module.exports = {
    handlers: (buildSchema, _, TodoType, TodoModel, log, LocationType, LocationModel) => {
        // This is the Root Query
        const TodoQueryRootType = new GraphQLObjectType({
            name: 'TodosSchema',
            description: 'This Schema will returns all todos stored in the app.',
            fields: () => ({
                // authors: {
                //     type: new GraphQLList(AuthorType),
                //     description: 'List of all Authors',
                //     resolve: function () {
                //         return Authors
                //     }
                // },
                todos: {
                    type: new GraphQLList(TodoType), // Tell the list its from type todo
                    description: 'It will return a list of all todos',
                    resolve: async () => { // gets called when the query is being executed
                        /**
                         * yey ES2017 is featured so we use it for finding all the models with mongoose.
                         * It's not necessary to try catch this. we can simply use catch from Promise library to log errors.
                         */
                        const todos = await TodoModel
                            .find({})
                            .catch(err => log.error(err));
                        // if (!todos) {
                        //     return [];
                        // }
                        return todos;
                    },

                },
                locations: {
                    type: new GraphQLList(LocationType), // Tell the list its from type todo
                    description: 'It will return a list of all locations',
                    resolve: async () => { // gets called when the query is being executed
                        /**
                         * yey ES2017 is featured so we use it for finding all the models with mongoose.
                         * It's not necessary to try catch this. we can simply use catch from Promise library to log errors.
                         */
                        const todos = await LocationModel
                            .find({})
                            .catch(err =>
                                log.error(err));
                        // if (!todos) {
                        //     return [];
                        // }
                        return todos;
                    }
                },
                // tasks: {

                // }
            })
        });
        // This is the schema declaration
        return new GraphQLSchema({
            query: TodoQueryRootType
            // If you need to create or updata a datasource, 
            // you use mutations. Note:
            // mutations will not be explored in this post.
            // mutation: BlogMutationRootType 
        });
    }
};