'use strict';
module.exports = {
    handlers: (baseController, taskModel, createHttpError, log) => {
        const tasksBaseController = baseController.createInstance(taskModel);

        /**
         * Route Handler for GET Requests on Tasks
         * NOTE: GetByID does not exists because simply set a filter to _id for filtering.
         * 
         * @param {Object} req Express Request
         * @param {Object} res Express Response
         * @param {Object} next Express next middleware
         */
        const getTasks = async (req, res, next) => {
            const filters = Object.keys(req.body) > 0 ?
                tasksBaseController.parseBody(req.body) : {};
            try {
                const result = await tasksBaseController.get(filters);
                res.status(200).json(result);
            } catch (err) {
                log.error(err);
                next(createHttpError(500, 'Cannot get Tasks'));
            }
        };


        /**
         * Route Handler for POST Requests on Tasks
         * 
         * @param {Object} req Express Request
         * @param {Object} res Express Response
         * @param {Object} next Express next middleware
         */
        const createTask = (req, res, next) => {
            const newTask = typeof req.body === 'object' ? req.body :
                tasksBaseController.parseBody(req.body);
            if (typeof newTask === 'undefined' || req.body === 'undefined') {
                return next(createHttpError(400, 'Body Cannot be Empty'));
            }
            tasksBaseController.create(newTask).then((result) => {
                res.status(200).json(result);
            }, (err) => {
                log.error(err);
                next(createHttpError(500, 'Cannot create Task'));
            });
        };

        /**
         * Route Handler for PUT Requests on Tasks
         * 
         * @param {Object} req Express Request
         * @param {Object} res Express Response
         * @param {Object} next Express next middleware
         */
        const editTask = (req, res, next) => {
            const existingTask = tasksBaseController.parseBody(req.body);
            if (typeof existingTask === 'undefined' || req.body === 'undefined') {
                return next(createHttpError(400, 'Body Cannot be Empty'));
            }
            tasksBaseController.edit(existingTask).then((result) => {
                res.status(200).json(result);
            }, (err) => {
                log.error(err);
                next(createHttpError(500, 'Cannot edit Task'));
            });
        };

        /**
         * Route Handler for DELETE Requests on Tasks
         * 
         * @param {Object} req Express Request
         * @param {Object} res Express Response
         * @param {Object} next Express next middleware
         */
        const deleteTask = (req, res, next) => {
            if (typeof req.params.uuid === 'undefined') {
                return next(createHttpError(400, 'uuid has to be defined in the params'));
            }
            tasksBaseController.delete(req.params.uuid).then((result) => {
                res.status(200).json(result);
            }, (err) => {
                log.error(err);
                next(createHttpError(500, 'Cannot edit Task'));
            });
        };

        return {
            getTasks,
            createTask,
            editTask,
            deleteTask
        };
    }
};