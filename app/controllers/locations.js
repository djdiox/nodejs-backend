'use strict';
module.exports = {
    handlers: (baseController, locationModel, createHttpError, log) => {
        const locationsBaseController = baseController.createInstance(locationModel);

        /**
         * Route Handler for GET Requests on Locations
         * NOTE: GetByID does not exists because simply set a filter to _id for filtering.
         * 
         * @param {Object} req Express Request
         * @param {Object} res Express Response
         * @param {Object} next Express next middleware
         */
        const getLocations = async (req, res, next) => {
            const filters = Object.keys(req.body) > 0 ?
                locationsBaseController.parseBody(req.body) : {};
            try {
                const result = await locationsBaseController.get(filters);
                res.status(200).json(result);
            } catch (err) {
                log.error(err);
                next(createHttpError(500, 'Cannot get Locations'));
            }
        };


        /**
         * Route Handler for POST Requests on Locations
         * 
         * @param {Object} req Express Request
         * @param {Object} res Express Response
         * @param {Object} next Express next middleware
         */
        const createLocation = (req, res, next) => {
            const newLocation = typeof req.body === 'object' ? req.body :
                locationsBaseController.parseBody(req.body);
            if (typeof newLocation === 'undefined' || req.body === 'undefined') {
                return next(createHttpError(400, 'Body Cannot be Empty'));
            }
            locationsBaseController.create(newLocation).then((result) => {
                res.status(200).json(result);
            }, (err) => {
                log.error(err);
                next(createHttpError(500, 'Cannot create Location'));
            });
        };

        /**
         * Route Handler for PUT Requests on Locations
         * 
         * @param {Object} req Express Request
         * @param {Object} res Express Response
         * @param {Object} next Express next middleware
         */
        const editLocation = (req, res, next) => {
            const existingLocation = locationsBaseController.parseBody(req.body);
            if (typeof existingLocation === 'undefined' || req.body === 'undefined') {
                return next(createHttpError(400, 'Body Cannot be Empty'));
            }
            locationsBaseController.edit(existingLocation).then((result) => {
                res.status(200).json(result);
            }, (err) => {
                log.error(err);
                next(createHttpError(500, 'Cannot edit Location'));
            });
        };

        /**
         * Route Handler for DELETE Requests on Locations
         * 
         * @param {Object} req Express Request
         * @param {Object} res Express Response
         * @param {Object} next Express next middleware
         */
        const deleteLocation = (req, res, next) => {
            if (typeof req.params.uuid === 'undefined') {
                return next(createHttpError(400, 'uuid has to be defined in the params'));
            }
            locationsBaseController.delete(req.params.uuid).then((result) => {
                res.status(200).json(result);
            }, (err) => {
                log.error(err);
                next(createHttpError(500, 'Cannot edit Location'));
            });
        };

        return {
            getLocations,
            createLocation,
            editLocation,
            deleteLocation
        };
    }
};