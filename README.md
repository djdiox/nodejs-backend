## Node.JS Backend

Build and developed with Node.JS and Mongoose.

## Requirements 

- [Node.JS 8.6.0](https://nodejs.org/en/)
- [MongoDB](https://www.mongodb.com/download-center?jmp=nav#community)

## API

API Description follows (Including GraphQL description)

## Credits

Created by Markus Wagner (c) 2017
Boilerplate by 2013 Madhusudhan Srinivasa <me@madhums.me>