'use strict';
/**
 * 
 * config/passport.js
 * 
 * This files configures the passport module, following these 
 * strategies:
 * - local
 * - jwt
 * - oAuth
 *   - facebook
 *   - twitter
 *   - google
 * 
 */

// Import strategies
const LocalStrategy = require('passport-local').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy,
    TwitterStrategy = require('passport-twitter').Strategy,
    GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
    JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt,
    config = require('./index'),
    User = require('../app/models/user'),
    log = require('winston'),
    passportCallbacks = require('./passport-callbacks')(User, log);


/**
 * A function will be returned, that takes passport as a parameter.
 * When invoked, this function sets up the passport configuration.
 * @param {any} passport module
 */
module.exports = (passport) => {

    passport.serializeUser((user, done) => {
        done(null, user);
    });

    passport.deserializeUser((user, done) => {
        done(null, user);
    });

    /* **********************************************************************
     *  LOCAL STRATEGY 
     ***********************************************************************/
    /**
     * Login
     */
    passport.use('local-login', new LocalStrategy({
        /**
         * By default, local strategy uses username and password, we 
         * will override username with email
         **/
        usernameField: 'email',
        passwordField: 'password',
        /**
         * Allows us to pass in the req from our route (lets us check if a 
         * user is logged in or not)
         */
        passReqToCallback: true
    }, passportCallbacks.localLogin));

    /**
     * Signup
     */
    passport.use('local-signup', new LocalStrategy({
        // Override the username by email as the login field.
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    }, passportCallbacks.localSignup));

    let tokenExtractor = (req) => {
        return req.query.token || ExtractJwt.fromAuthHeader()(req);
    };

    /* **********************************************************************
     * JWT TOKEN STRATEGY 
     ***********************************************************************/
    passport.use(new JwtStrategy({
        secretOrKey: config.secretKey,
        jwtFromRequest: tokenExtractor
    }, passportCallbacks.tokenCb));

    /* **********************************************************************
     * FACEBOOK STRATEGY
     ***********************************************************************/
    passport.use(new FacebookStrategy({
        clientID: config.facebook.clientID,
        clientSecret: config.facebook.clientSecret,
        callbackURL: config.facebook.callbackURL,
        passReqToCallback: true
    }, passportCallbacks.facebook));


    /* **********************************************************************
     * TWITTER STRATEGY
     ***********************************************************************/
    passport.use(new TwitterStrategy({
        consumerKey: config.twitter.consumerKey,
        consumerSecret: config.twitter.consumerSecret,
        callbackURL: config.twitter.callbackURL,
        passReqToCallback: true
    }, passportCallbacks.twitter));

    /* **********************************************************************
     * GOOGLE STRATEGY
     ***********************************************************************/
    passport.use(new GoogleStrategy({
        clientID: config.google.clientID,
        clientSecret: config.google.clientSecret,
        callbackURL: config.google.callbackURL,
        passReqToCallback: true
    }, passportCallbacks.google));

};