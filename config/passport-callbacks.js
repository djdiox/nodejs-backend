'use strict';
/**
 * Creates all needed callback functions for integrations by sso e.g. google, twitter facebook
 * 
 * @param {any} User An Instance of the User Model from mongoose
 * @param {any} log an log Element
 * @returns 
 */
module.exports = (User, log) => {
  const saveUser = (currentUser, strategyName, userInformation, done) => {
    currentUser[strategyName] = userInformation;
    if (currentUser.hasOwnProperty('name') === false) {
      currentUser.name = userInformation.email;
    }
    if (userInformation && userInformation.hasOwnProperty('password')) {
      currentUser.hashed_password = userInformation.password;
    } else if (strategyName !== 'local' && currentUser.hasOwnProperty('hashed_password') === false) {
      currentUser.hashed_password = User.generatePassword('thisIsMySecret1993!dfjfgnj'); // in case we dont have a local accout yet
    }
    return currentUser.save((err) => {
      if (err) throw err;
      return done(null, currentUser);
    });
  };
  const createUserFromCallback = (
    strategyName,
    user,
    userInformation,
    profile,
    done
  ) => {
    if (user && profile) {
      const filter = {};
      filter[strategyName + '.id'] = profile.id;
      return User.findOne(filter, (err, res) => {
        if (err) {
          return done(err);
        }
        return saveUser(res || user, strategyName, userInformation, done);
      });
    }
    const currentUser = new User(userInformation);
    return saveUser(currentUser, strategyName, userInformation, done);
  };
  const callbacks = {
    /**
     * @function tokenCb - Token strategy callback
     * Called after verifying a tokens validity. The user's id will be
     * specified into the token's payload.
     * @param {any} jwt_payload
     * @param {any} done
     */
    tokenCb: (jwt_payload, done) => {
      User.findOne({
          _id: jwt_payload._id
        },
        (err, user) => {
          if (err) log.error(err);
          done(err, user);
        }
      );
    },
    localLogin: (req, email, password, done) => {
      // Async: let the machine do its stuff.
      process.nextTick(() => {
        log.info('New User trying to login to Server', email);
        log.debug('More Infos', req);
        User.findOne({
            'local.email': email
          },
          (err, user) => {
            if (err) {
              log.error('Error at finding User', err, user);
              return done(err);
            }
            // If no user is found, return the message
            if (!user) {
              log.info('User not found');
              return done(null, false, 'No user has been found.');
            }
            if (!user.validPassword(password)) {
              log.debug('Wrong password');
              return done(null, false, 'Wrong password.');
              // If everything went fine, return the user
            } else {
              log.info('Succesfull login');
              log.debug('More infos ', user);
              return done(null, user);
            }
          }
        );
      });
    },
    localSignup: (req, email, password, done) => {
      // Async: let the machine do its stuff.
      process.nextTick(() => {
        //  Whether we're signing up or connecting an account, we'll need
        //  to know if the email address is in use.
        User.findOne({
            'local.email': email
          },
          (err, existingUser) => {
            if (err) return done(err);
            /*
             * Return an error when the email is already taken by another user.
             */
            if (existingUser) {
              return done(
                null,
                false,
                'There already exists an account with that email.'
              );
            }
            /*
             * When the user has already been logged in (using a different strategy),
             * the local login for this account will be set up now.
             */
            createUserFromCallback(
                'local',
                req.user, {
                    email,
                    password: User.generatePassword(password)
                },
                null,
                done
            );
            //   if (req.user) {
            //     let user = req.user;
            //     user.local.email = email;
            //     user.local.password = user.generateHash(password);
            //     user.save(function (err) {
            //       if (err)
            //         throw err;
            //       return done(null, user);
            //     });
            //     return;
            //   }
            //   //  We're not logged in, so we're creating a brand new user.
            //   let newUser = new User();
            //   newUser.local.email = email;
            //   newUser.local.password = newUser.generateHash(password);
            //   newUser.save(function (err) {
            //     if (err)
            //       throw err;
            //     return done(null, newUser);
            //   });
          }
        );
      });
    },
    facebook: (req, token, refreshToken, profile, done) => {
      // Async: let the machine do its stuff.
      process.nextTick(() => {
        // check if the user is already logged in
        const email = profile.emails ? profile.emails[0].value : '';
        const name = profile.hasOwnProperty('name') ? profile.name.givenName + ' ' + profile.name.familyName :
          profile.displayName;

        createUserFromCallback('facebook', new User({
          name: name,
          email
        }), {
          email,
          name,
          token: profile.token || token
        }, profile, done);
        //     User.findOne({
        //       'facebook.id': profile.id
        //     },
        //       function (err, user) {
        //         if (err) return done(err);
        //         // If there is a user id already but no token
        //         // (user was linked at one point and then removed)

        //         // if (user) {
        //         //   if (!user.facebook.token) {
        //         //     user.facebook.token = token;
        //         //     user.facebook.name = profile.name.givenName + ' ' + profile.name.familyName;
        //         //     user.facebook.email = profile.emails[0].value;
        //         //     // The user will be updated and saved again.
        //         //     user.save(function (err) {
        //         //       if (err) throw err;
        //         //     });
        //         //   }
        //         //   return done(null, user); // user found, return that user
        //         // } else {
        //         //   // If there is no user, create it now.
        //         //   let newUser = new User();
        //         //   newUser.facebook.id = profile.id;
        //         //   newUser.facebook.token = token;
        //         //   newUser.facebook.name = profile.displayName;
        //         //   newUser.facebook.email = profile.emails ? profile.emails[0].value : '';

        //         //   newUser.save(function (err) {
        //         //     if (err)
        //         //       throw err;
        //         //     return done(null, newUser);
        //         //   });
        //         //   return;
        //         // }
        //       });
        //     return;
        //   }
        //   /**
        //    * The user already exists and is logged in, we have to link accounts.
        //    */
        //   let user = req.user; // pull the user out of the session

        //   user.facebook.id = profile.id;
        //   user.facebook.token = token;
        //   user.facebook.name = profile.name.givenName + ' ' + profile.name.familyName;
        //   user.facebook.email = profile.emails[0].value;

        //   user.save(function (err) {
        //     if (err)
        //       throw err;
        //     return done(null, user);
        //   });
      });
    },
    twitter: (req, token, tokenSecret, profile, done) => {
      // Async: let the machine do its stuff.
      process.nextTick(() => {
        // Check if the user is already logged in
        createUserFromCallback(
          'twitter',
          req.user, {
            mail: profile.emails ? profile.emails[0].value : '',
            username: profile.username,
            displayName: profile.displayName,
            token: profile.token || token
          },
          profile,
          done
        );
        //   if (!req.user) {
        //     User.findOne({
        //       'twitter.id': profile.id
        //     }, function (err, user) {
        //       if (err) return done(err);
        //       /**
        //        * If there is a user id already but no token (user
        //        * was linked at one point and then removed)
        //        */
        //       if (user) {
        //         if (!user.twitter.token) {
        //           user.twitter.token = token;
        //           user.twitter.username = profile.username;
        //           user.twitter.displayName = profile.displayName;

        //           user.save(function (err) {
        //             if (err)
        //               throw err;
        //             return;
        //           });
        //         }

        //         return done(null, user); // User found, return that user
        //       } else {
        //         // if there is no user, create them
        //         let newUser = new User();

        //         newUser.twitter.id = profile.id;
        //         newUser.twitter.token = token;
        //         newUser.twitter.username = profile.username;
        //         newUser.twitter.displayName = profile.displayName;

        //         newUser.save(function (err) {
        //           if (err)
        //             throw err;
        //           return done(null, newUser);
        //         });
        //       }
        //     });
        //     return;
        //   }
        //   // user already exists and is logged in, we have to link accounts
        //   let user = req.user; // pull the user out of the session

        //   user.twitter.id = profile.id;
        //   user.twitter.token = token;
        //   user.twitter.username = profile.username;
        //   user.twitter.displayName = profile.displayName;

        //   user.save(function (err) {
        //     if (err)
        //       throw err;
        //     return done(null, user);
        //   });
      });
    },
    google: (req, token, refreshToken, profile, done) => {
      // Async: let the machine do its stuff.
      process.nextTick(() => {
        createUserFromCallback(
          'google',
          req.user, {
            mail: profile.emails ? profile.emails[0].value : '',
            username: profile.username,
            displayName: profile.displayName,
            token: profile.token || token
          },
          profile,
          done
        );
        // Check if the user is already logged in
        // if (!req.user) {

        //   User.findOne({
        //     'google.id': profile.id
        //   }, function (err, user) {
        //     if (err) return done(err);
        //     // If there is a user id already but no token (user was linked at one point and then removed)
        //     if (user) {
        //       if (!user.google.token) {
        //         user.google.token = token;
        //         user.google.name = profile.displayName;
        //         user.google.email = profile.emails[0].value; // pull the first email
        //         user.save(function (err) {
        //           if (err)
        //             throw err;
        //           return;
        //         });
        //       }
        //       return done(null, user); // Here is where the user must be returned.
        //     }
        //     let newUser = new User();

        //     newUser.google.id = profile.id;
        //     newUser.google.token = token;
        //     newUser.google.name = profile.displayName;
        //     newUser.google.email = profile.emails[0].value; // pull the first email

        //     newUser.save(function (err) {
        //       if (err)
        //         throw err;
        //       return done(null, newUser);
        //     });

        //   });
        //   return;
        // }
        // // user already exists and is logged in, we have to link accounts
        // User.findOne({
        //   'id': req.user.id
        // }, (err, user) => {
        //   if (err) return done(err);

        //   user.google = {};
        //   user.google.id = profile.id;
        //   user.google.token = token;
        //   user.google.name = profile.displayName;
        //   user.google.email = profile.emails[0].value; // pull the first email
        //   user.save(function (err) {
        //     if (err)
        //       throw err;
        //     return done(null, user);
        //   });
        // });
      });
    }
  };
  return callbacks;
};