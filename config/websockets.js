'use strict';
module.exports = {
    handlers: (server) => {
        const io = require('socket.io')(server);

        const initConnection = () => {
            io.on('connection', function (socket) {
                socket.emit('news', {
                    hello: 'world'
                });
                socket.on('my other event', function (data) {
                    console.log(data);
                });
            });

            return {
                initConnection
            };
        };
    }
};