"use strict";
/**
 * Expose
 */

module.exports = {
  port: 3015,
  mongoUrl: process.env.MONGO_URL || 'mongodb://localhost:27017/dashboardDB_Test',
  secretKey: "secretKey2017!",
  logLevel: "debug",
  baseUrl: "http://localhost:3000",
  facebook: {
    clientID: "testFb",
    clientSecret: "clientSecretFb",
    callbackURL: "http://localhost:3000/auth/facebook/callback"
  },
  twitter: {
    clientID: "testTwitter",
    clientSecret: "clientSecretTwitter",
    consumerKey: "test123",
    consumerSecret: "test123",
    callbackURL: "http://localhost:3000/auth/twitter/callback"
  },
  github: {
    clientID: "testGithub",
    clientSecret: "clientSectetGitHub",
    callbackURL: "http://localhost:3000/auth/github/callback"
  },
  google: {
    clientID: "testGoogle",
    clientSecret: "secretTwitter",
    callbackURL: "http://localhost:3000/auth/google/callback"
  },
  spotify: {
    clientId: "testSpotify",
    clientSecret: "secretSpotify",
    callbackURL: "http://localhost:3000/spotify"
  }
};
