'use strict';
// const test = require('tape');
const sinon = require('sinon');
const chai = require('chai');
const expect = chai.expect;
const passportHandler = require('../../config/passport');
describe('The Passport Configurator.', () => {

    const sandbox = sinon.sandbox.create();

    const passportMock = {
        serializeUser: sandbox.spy(),
        deserializeUser: sandbox.spy(),
        use: sandbox.spy()
    };

    it('should call needed functions', () => {
        passportHandler(passportMock);
        expect(passportMock.deserializeUser.called).to.be.ok;
        expect(passportMock.serializeUser.called).to.be.ok;
        expect(passportMock.use.called).to.be.ok;
    });
});
