const chai = require('chai'),
    expect = chai.expect,
    sinon = require('sinon');

describe('it should initialize the middlewares', () => {

    const sandbox = sinon.sandbox.create();
    const morganMock = sandbox.spy();
    const expressMock = {
        static: sandbox.spy()
    };
    const winstonMock = {
        info: sandbox.spy()
    };
    const sessionMock = sandbox.spy();
    const compressionMock = sandbox.spy();
    const cookieParseMock = sandbox.spy();
    const cookieSessionMock = sandbox.spy();
    const bodyParserMock = {
        urlencoded: sandbox.spy()
    };
    const methodOverrideMock = sandbox.spy();
    const csrfMock = {

    };
    const mongoStoreMock = {

    };
    const flashMock = sandbox.spy();
    const helpersMock = sandbox.spy();
    const jadeMock = {};
    const configMock = {
        mongoUrl:'test'
    };
    const pkgMock = {
        name:'nodejs-backend'
    };
    const corsMock = sandbox.spy();
    const middlewareBootstrap = require('../../config/middleware')
        .handlers(expressMock, sessionMock, compressionMock, morganMock,
            cookieParseMock, cookieSessionMock, bodyParserMock, methodOverrideMock,
            csrfMock, mongoStoreMock, flashMock, winstonMock, helpersMock, jadeMock,
            configMock, pkgMock, corsMock);
    it('should get an logger for http requests, or just logger', () => {
        const logger = middlewareBootstrap.getLoggerForHttpRequests('test');
        expect(logger).to.equal('dev');
    });
    it('should initialize all the needed middlewares for the app', () => {

        const appMock = {
            use: sinon.spy()
        };
        const passportMock = {
            initialize: sinon.spy(),
            session: sinon.spy()
        };
        middlewareBootstrap.bootstrapMiddleware(appMock, passportMock);
    });
});