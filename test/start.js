'use strict';

/*
 * Module dependencies.
 */

// const test = require('tape');
// const sinon = require('sinon');
const { app } = require('../server');
const chai = require('chai');
const expect = chai.expect;
describe('The Server.', () => {

    it('should initialize', () => {
        expect(app).not.to.be.undefined;
    });
});

// test.onFinish(() => process.exit(0));
